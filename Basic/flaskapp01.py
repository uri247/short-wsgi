import os
import sys

from flask import Flask, render_template

application = Flask('flasksrv')

@application.route('/')
@application.route('/<name>')
def hello(name=None):
    #return render_template('hello.html')
    return 'Hello, %s' % name