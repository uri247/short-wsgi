import os
import sys

from flask import Flask, render_template

application = Flask('flasksrv')

@application.route('/')
@application.route('/<name>')
def hello(name=None):
    user = { 'nickname': name }
    return render_template(
        'hello.html',
        title = 'Hello Flask',
        user = user
    )
