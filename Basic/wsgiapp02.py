#! /usr/bin/env python
import wsgiref.simple_server


def application(environ, start_response):
    """This is the application method. It can be name anything, but the canonical name is
    'application'
    :param environ:
    :param start_response:
    :return: an iterator with all the content
    """

    lines = ['%s: %s\n' % (key, value) for key, value in sorted(environ.items())]
    length = sum(len(line) for line in lines)

    status = '200 OK'
    response_header = [
        ('Content-Type', 'text/plain'),
        ('Content-Length', str(length)),
    ]

    start_response(status, response_header)
    return lines


httpd = wsgiref.simple_server.make_server('localhost', 8080, application)
httpd.serve_forever()
