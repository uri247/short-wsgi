#! /usr/bin/env python

import wsgiref.simple_server


def application(environ, start_response):
    """This is the application method. It can be name anything, but the canonical name is
    'application'

    :param environ:
    :param start_response:
    :return: an iterator with all the content
    """
    response_body = 'The request method is: %s' % environ['REQUEST_METHOD']
    status = '200 OK'

    response_header = [
        ('Content-Type', 'text/plain'),
        ('Content-Length', str(len(response_body))),
    ]

    start_response(status, response_header)
    return response_body

httpd = wsgiref.simple_server.make_server('localhost', 8080, application)
httpd.serve_forever()