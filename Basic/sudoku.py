
def all_unique(itr):
    seen = [0] * 9
    for x in itr:
        seen[x-1] += 1
    return all(seen)

def box_range():
    for x in xrange(3):
        for y in xrange(3):
            yield (x, y)

class Table(object):
    def __init__(self, data):
        self.__table_data = data
        pass

    def cell(self, r, c):
        return self.__table_data[r][c]

    def row(self, r):
        for c in xrange(9):
            yield self.cell(r, c)

    def col(self, c):
        for r in xrange(9):
            yield self.cell(r,c)

    def box(self, R, C):
        for r,c in box_range():
            yield self.cell(R*3+r, C*3+c)

    def verify_solution(self):
        row_check = [all_unique(self.row(r)) for r in xrange(9)]
        col_check = [all_unique(self.col(c)) for c in xrange(9)]
        box_check = [all_unique(self.box(R,C)) for R,C in box_range()]

        has_error = False

        for name, arr in [('row', row_check), ('col', col_check), ('box', box_check)]:
            for i, f in enumerate(arr):
                if not f:
                    print 'error at %s %d' % (name, i)
                    has_error = True

        if not has_error:
            print 'table is good'



def main():
    table_data = [
        [8, 2, 7, 1, 5, 4, 3, 9, 6],
        [9, 6, 5, 3, 2, 7, 1, 4, 8],
        [3, 4, 1, 6, 8, 9, 7, 5, 2],
        [5, 9, 3, 4, 6, 8, 2, 7, 1],
        [4, 7, 2, 5, 1, 3, 6, 8, 9],
        [6, 1, 8, 9, 7, 2, 4, 3, 5],
        [7, 8, 6, 2, 3, 5, 9, 1, 4],
        [1, 5, 4, 7, 9, 6, 8, 2, 3],
        [2, 3, 9, 8, 4, 1, 5, 6, 7],
    ]

    table = Table(table_data)
    table.verify_solution()

    table_data[1][3] += 1
    bad_table = Table(table_data)
    table.verify_solution()


if __name__ == '__main__':
    main()